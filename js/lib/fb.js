define(['facebook'], function(){
    'use strict';
    
    FB.init({
        appId      : 'YOUR_APP_ID'
    });
    FB.getLoginStatus(function(response) {
        console.log(response);
    });
});