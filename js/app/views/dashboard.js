define(['jquery', 'app/config', 'handlebars', 'text!templates/usersGrid.html'],
function($, config, Handlebars, template){
    'use strict';
    
    function render(data){
        var _template = Handlebars.compile(template);
        var listHtml = _template(data),
            $userList = $(listHtml);
        
        $userList.find('.item').on('click', function(){
            alert($(this).find('.uid').text());
        });

        $(config.$view).html($userList);
        
        return $userList;
    }

    return {
        render:render
    };
});