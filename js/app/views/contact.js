define(['jquery', 'app/config', 'handlebars', 'text!templates/contactPage.html'],
function($, config, Handlebars, template){
    'use strict';
    
    function render(){
        var _template = Handlebars.compile(template);
        var html = _template();
        $(config.$view).html($(html));
    }

    return {
        render:render
    };
});