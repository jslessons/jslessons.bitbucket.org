define(['auth', 'vkontakte'], function(auth, VK){
    'use strict';

    function getFriends(callback){
        var session = auth.getSession();
        if(session){
            VK.Api.call('friends.get', {
                uids: session.mid,
                fields: 'first_name, last_name, photo, online'
            },
            function(data) {
                if(data.response) {
                    callback(data.response);
                };
            });
        } else {
            callback(null);
        };
    };
    
    function getUsers(uids, callback){
        VK.Api.call('users.get', {uids: uids, fields: 'first_name, last_name, photo, online'} ,
        function(data) {
            if(data.response) {
                callback(data.response);
            }
        });
    };

    return {
        getUsers: getUsers,
        getFriends: getFriends
    };
});