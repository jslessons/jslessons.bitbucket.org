// Модуль библиотека
define(['jquery', 'app/router', 'handlebars', 'text!templates/swipeMenu.html' ],
function($, router, Handlebars, template){
    'use strict';

    var showed = false,
        template = Handlebars.compile(template),
        menuHtml = template({
            items: router.getRoutes()
        }),
        $menu = $(menuHtml);

    $('body').append($menu);

    function toggleMenu(){
        if(showed){
            hide();
        } else {
            show();
        }
    };

    return {
        toggle: toggleMenu
    }
    
    // private methods
    function show(){
        showed = true;
        $('html').addClass('activeMenu');
    };

    function hide(){
        showed = false;
        $('html').removeClass('activeMenu');
    };
});