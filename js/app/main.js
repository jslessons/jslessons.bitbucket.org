define(['auth', 'app/router', 'app/components/toolbar'], 
function(auth, router, toolbar){
    'use strict';
    
    console.log('Выполняем какие то common действия (/js/app/main.js)');
    auth.checkStatus(function(){
        router.startRouting();
    });
});
